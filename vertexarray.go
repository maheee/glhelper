package glhelper

import (
	gl "gogl"
)

type VertexArray struct {
	location gl.Uint
}

func MakeVertexArray() (vArr *VertexArray, err error) {
	vArr = &VertexArray{}
	gl.GenVertexArrays(1, &vArr.location)
	return
}

func (vArr *VertexArray) Bind() {
	gl.BindVertexArray(vArr.location)
}

func (vArr *VertexArray) Unbind() {
	gl.BindVertexArray(0)
}

func (vArr *VertexArray) Delete() {
	gl.DeleteVertexArrays(1, &vArr.location)
}

func (vArr *VertexArray) Setup(method func()) {
	vArr.Bind()
	method()
	vArr.Unbind()
}
