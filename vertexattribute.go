package glhelper

import (
	gl "gogl"
)

type VertexAttribute struct {
	location gl.Uint
}

func (program *Program) GetVertexAttribute(name string) (*VertexAttribute, error) {
	nameGl := gl.GLString(name)
	defer gl.GLStringFree(nameGl)
	return &VertexAttribute{location: gl.Uint(int(gl.GetAttribLocation(program.location, nameGl)))}, nil
}

func (attribute *VertexAttribute) Enable() {
	gl.EnableVertexAttribArray(attribute.location)
}

func (attribute *VertexAttribute) Disable() {
	gl.DisableVertexAttribArray(attribute.location)
}

func (attribute *VertexAttribute) BindIntBuffer(buffer *IntBuffer) {
	buffer.Bind()
	gl.VertexAttribPointer(attribute.location, gl.Int(buffer.partSize),
		gl.INT, gl.FALSE, 0, nil)
	buffer.Unbind()
}

func (attribute *VertexAttribute) BindFloatBuffer(buffer *FloatBuffer) {
	buffer.Bind()
	gl.VertexAttribPointer(attribute.location, gl.Int(buffer.partSize),
		gl.FLOAT, gl.FALSE, 0, nil)
	buffer.Unbind()
}
