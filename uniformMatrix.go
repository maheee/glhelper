package glhelper

import (
	gl "gogl"
)

/*
 * Special Types
 */
/*2*/
type UniformMatrix2x2f Uniform
type UniformMatrix2x3f Uniform
type UniformMatrix2x4f Uniform
type UniformMatrix3x2f Uniform
type UniformMatrix3x3f Uniform
type UniformMatrix3x4f Uniform
type UniformMatrix4x2f Uniform
type UniformMatrix4x3f Uniform
type UniformMatrix4x4f Uniform

/*
 * Single Matrix Setter
 */

func (u *UniformMatrix2x2f) Set(value [2][2]gl.Float) {
	gl.UniformMatrix2fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix3x2f) Set(value [2][3]gl.Float) {
	gl.UniformMatrix3x2fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix4x2f) Set(value [2][4]gl.Float) {
	gl.UniformMatrix4x2fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix2x3f) Set(value [2][3]gl.Float) {
	gl.UniformMatrix2x3fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix3x3f) Set(value [3][3]gl.Float) {
	gl.UniformMatrix3fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix4x3f) Set(value [3][4]gl.Float) {
	gl.UniformMatrix4x3fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix2x4f) Set(value [4][2]gl.Float) {
	gl.UniformMatrix2x4fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix3x4f) Set(value [4][2]gl.Float) {
	gl.UniformMatrix3x4fv(u.location, 1, gl.TRUE, &value[0][0])
}

func (u *UniformMatrix4x4f) Set(value [4][4]gl.Float) {
	gl.UniformMatrix4fv(u.location, 1, gl.TRUE, &value[0][0])
}

/*
 * Multi Matrix Setter
 */

func (u *UniformMatrix2x2f) SetArray(values [][2][2]gl.Float) {
	gl.UniformMatrix2fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix3x2f) SetArray(values [][2][3]gl.Float) {
	gl.UniformMatrix3x2fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix4x2f) SetArray(values [][2][4]gl.Float) {
	gl.UniformMatrix4x2fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix2x3f) SetArray(values [][2][3]gl.Float) {
	gl.UniformMatrix2x3fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix3x3f) SetArray(values [][3][3]gl.Float) {
	gl.UniformMatrix3fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix4x3f) SetArray(values [][3][4]gl.Float) {
	gl.UniformMatrix4x3fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix2x4f) SetArray(values [][4][2]gl.Float) {
	gl.UniformMatrix2x4fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix3x4f) SetArray(values [][4][2]gl.Float) {
	gl.UniformMatrix3x4fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

func (u *UniformMatrix4x4f) SetArray(values [][4][4]gl.Float) {
	gl.UniformMatrix4fv(u.location, gl.Sizei(len(values)), gl.TRUE, &values[0][0][0])
}

/*
 * Special Getter
 */
func (program *Program) GetUniformMatrix2x2f(name string) (*UniformMatrix2x2f, error) {
	return &UniformMatrix2x2f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix3x2f(name string) (*UniformMatrix3x2f, error) {
	return &UniformMatrix3x2f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix4x2f(name string) (*UniformMatrix4x2f, error) {
	return &UniformMatrix4x2f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix2x3f(name string) (*UniformMatrix2x3f, error) {
	return &UniformMatrix2x3f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix3x3f(name string) (*UniformMatrix3x3f, error) {
	return &UniformMatrix3x3f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix4x3f(name string) (*UniformMatrix4x3f, error) {
	return &UniformMatrix4x3f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix2x4f(name string) (*UniformMatrix2x4f, error) {
	return &UniformMatrix2x4f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix3x4f(name string) (*UniformMatrix3x4f, error) {
	return &UniformMatrix3x4f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniformMatrix4x4f(name string) (*UniformMatrix4x4f, error) {
	return &UniformMatrix4x4f{program.location, name, getUniformLocation(program, name)}, nil
}
