package glhelper

import (
	"bytes"
	"errors"
	gl "gogl"
	"image"
	"image/png"
	"io"
	"io/ioutil"
)

type Texture struct {
	location gl.Uint
}

func (texture *Texture) BindTo(ch int) {
	gl.ActiveTexture(gl.Enum(gl.TEXTURE0 + ch))
	gl.BindTexture(gl.TEXTURE_2D, texture.location)
}

func MakeTexture(filename string) (*Texture, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	textureLocation, err := makeTextureFromBytes(data)
	if err != nil {
		return nil, err
	}
	return &Texture{location: textureLocation}, nil
}

func (texture *Texture) Delete() {
	gl.DeleteTextures(1, &texture.location)
}

/*
 * Internals
 */

func makeTextureFromBytes(data []byte) (gl.Uint, error) {
	r := bytes.NewBuffer(data)
	return makeTexture(r)
}

func makeTexture(r io.Reader) (textureId gl.Uint, err error) {
	data, imgWidth, imgHeight, err := getImageData(r)
	if err != nil {
		return 0, err
	}

	gl.GenTextures(1, &textureId)
	gl.BindTexture(gl.TEXTURE_2D, textureId)

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGB8,
		gl.Sizei(imgWidth), gl.Sizei(imgHeight), 0,
		gl.RGBA, gl.UNSIGNED_BYTE,
		gl.Pointer(&data[0]))

	return
}

func getImageData(r io.Reader) ([]byte, int, int, error) {
	img, err := png.Decode(r)
	if err != nil {
		return nil, 0, 0, err
	}

	rgbaImg, ok := img.(*image.NRGBA)
	if !ok {
		return nil, 0, 0, errors.New("texture must be an NRGBA image")
	}

	// flip image: first pixel is lower left corner
	imgWidth, imgHeight := img.Bounds().Dx(), img.Bounds().Dy()
	data := make([]byte, imgWidth*imgHeight*4)
	lineLen := imgWidth * 4
	dest := len(data) - lineLen
	for src := 0; src < len(rgbaImg.Pix); src += rgbaImg.Stride {
		copy(data[dest:dest+lineLen], rgbaImg.Pix[src:src+rgbaImg.Stride])
		dest -= lineLen
	}

	return data, imgWidth, imgHeight, nil
}
