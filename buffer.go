package glhelper

import (
	gl "gogl"
	"unsafe"
)

type IntBuffer struct {
	location gl.Uint
	target   gl.Enum
	typ      gl.Enum
	partSize int
}

type FloatBuffer struct {
	location gl.Uint
	target   gl.Enum
	typ      gl.Enum
	partSize int
}

func MakeIntBuffer(target gl.Enum, typ gl.Enum) (*IntBuffer, error) {
	return &IntBuffer{makeBuffer(), target, typ, -1}, nil
}

func MakeFloatBuffer(target gl.Enum, typ gl.Enum) (*FloatBuffer, error) {
	return &FloatBuffer{makeBuffer(), target, typ, -1}, nil
}

func (buffer *IntBuffer) SetPartSize(partSize int) {
	buffer.partSize = partSize
}

func (buffer *FloatBuffer) SetPartSize(partSize int) {
	buffer.partSize = partSize
}

func (buffer *IntBuffer) Bind() {
	gl.BindBuffer(buffer.target, buffer.location)
}

func (buffer *FloatBuffer) Bind() {
	gl.BindBuffer(buffer.target, buffer.location)
}

func (buffer *IntBuffer) Unbind() {
	gl.BindBuffer(buffer.target, 0)
}

func (buffer *FloatBuffer) Unbind() {
	gl.BindBuffer(buffer.target, 0)
}

func (buffer *IntBuffer) Delete() {
	gl.DeleteBuffers(1, &buffer.location)
}

func (buffer *FloatBuffer) Delete() {
	gl.DeleteBuffers(1, &buffer.location)
}

func (buffer *IntBuffer) Update(data []gl.Int, partSize int) error {
	buffer.partSize = partSize
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *IntBuffer) Update1(data [][1]gl.Int) error {
	buffer.partSize = 1
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *IntBuffer) Update2(data [][2]gl.Int) error {
	buffer.partSize = 2
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *IntBuffer) Update3(data [][3]gl.Int) error {
	buffer.partSize = 3
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *IntBuffer) Update4(data [][4]gl.Int) error {
	buffer.partSize = 4
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *FloatBuffer) Update(data []gl.Float, partSize int) error {
	buffer.partSize = partSize
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *FloatBuffer) Update1(data [][1]gl.Float) error {
	buffer.partSize = 1
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *FloatBuffer) Update2(data [][2]gl.Float) error {
	buffer.partSize = 2
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *FloatBuffer) Update3(data [][3]gl.Float) error {
	buffer.partSize = 3
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func (buffer *FloatBuffer) Update4(data [][4]gl.Float) error {
	buffer.partSize = 4
	return updateBuffer(buffer.target, buffer.typ, buffer.location,
		gl.Sizeiptr(int(unsafe.Sizeof(data[0]))*len(data)),
		gl.Pointer(&data[0]))
}

func makeBuffer() (buffer gl.Uint) {
	gl.GenBuffers(1, &buffer)
	return
}

func updateBuffer(target gl.Enum, typ gl.Enum, buffer gl.Uint, bufferSize gl.Sizeiptr, bufferData gl.Pointer) error {
	gl.BindBuffer(target, buffer)
	gl.BufferData(target, bufferSize, bufferData, typ)
	return nil
}
