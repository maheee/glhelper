package math

import (
	gl "gogl"
	. "math"
)

type SpaceMatrix struct {
	matrix *[4][4]gl.Float
}

func (sm SpaceMatrix) Matrix() [4][4]gl.Float {
	return *sm.matrix
}

func CreateMatrix(m *[4][4]gl.Float) SpaceMatrix {
	return SpaceMatrix{m}
}

func CreateUnityMatrix() SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{1.0, 0.0, 0.0, 0.0},
		{0.0, 1.0, 0.0, 0.0},
		{0.0, 0.0, 1.0, 0.0},
		{0.0, 0.0, 0.0, 1.0},
	}}
}

func CreateTranslationMatrix(x, y, z gl.Float) SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{1.0, 0.0, 0.0, x},
		{0.0, 1.0, 0.0, y},
		{0.0, 0.0, 1.0, z},
		{0.0, 0.0, 0.0, 1.0},
	}}
}

func CreateScaleMatrix(sx, sy, sz gl.Float) SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{sx, 0.0, 0.0, 0.0},
		{0.0, sy, 0.0, 0.0},
		{0.0, 0.0, sz, 0.0},
		{0.0, 0.0, 0.0, 1.0},
	}}
}

func CreateRotationMatrix(x, y, z gl.Float) SpaceMatrix {
	var a = float64(x)
	var b = float64(y)
	var c = float64(z)
	return SpaceMatrix{&[4][4]gl.Float{
		{
			gl.Float(Cos(b) * Cos(c)),
			gl.Float(Cos(b) * Sin(c)),
			gl.Float(-Sin(b)),
			0},
		{
			gl.Float(Sin(a)*Sin(b)*Cos(c) - Cos(a)*Sin(c)),
			gl.Float(Sin(a)*Sin(b)*Sin(c) + Cos(a)*Cos(c)),
			gl.Float(Sin(a) * Cos(b)),
			0},
		{
			gl.Float(Cos(a)*Sin(b)*Cos(c) + Sin(a)*Sin(c)),
			gl.Float(Cos(a)*Sin(b)*Sin(c) - Sin(a)*Cos(c)),
			gl.Float(Cos(a) * Cos(b)),
			0},
		{
			0,
			0,
			0,
			1},
	}}
}

func CreateMirrorMatrix(x, y, z gl.Float) SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{
			1 - 2*x*x,
			-2 * x * y,
			-2 * x * z,
			0},
		{
			-2 * x * y,
			1 - 2*y*y,
			-2 * y * z,
			0},
		{
			-2 * x * z,
			-2 * y * z,
			1 - 2*z*z,
			0},
		{
			0,
			0,
			0,
			1},
	}}
}

func CreateOrthogonalProjectionMatrix(left, top, right, bottom, near, far gl.Float) SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{2 / (right - left), 0.0, 0.0, -((right + left) / (right - left))},
		{0.0, 2 / (top - bottom), 0.0, -((top + bottom) / (top - bottom))},
		{0.0, 0.0, -2 / (far - near), -((far + near) / (far - near))},
		{0.0, 0.0, 0.0, 1.0},
	}}
}

func CreatePerspectiveProjectionMatrix(width, height, near, far gl.Float) SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{-(2 * near) / width, 0.0, 0.0, 0.0},
		{0.0, -(2 * near) / height, 0.0, 0.0},
		{0.0, 0.0, (far + near) / (near - far), (2 * far * near) / (far - near)},
		{0.0, 0.0, -1.0, 0.0},
	}}
}

func CreatePerspectiveProjectionMatrix2(width, height, near, far gl.Float) SpaceMatrix {
	return SpaceMatrix{&[4][4]gl.Float{
		{(2 * near) / width, 0.0, 0.0, 0.0},
		{0.0, (2 * near) / height, 0.0, 0.0},
		{0.0, 0.0, (far + near) / (near - far), (2 * far * near) / (far - near)},
		{0.0, 0.0, -1.0, 0.0},
	}}
}

func (m1 SpaceMatrix) Clean() SpaceMatrix {
	for x := 0; x < 4; x++ {
		for y := 0; y < 4; y++ {
			m1.matrix[y][x] /= m1.matrix[3][3]
		}
	}

	return m1
}

func (m1 SpaceMatrix) Mult(m2 *SpaceMatrix) SpaceMatrix {
	m1c := *m1.matrix

	for x := 0; x < 4; x++ {
		for y := 0; y < 4; y++ {
			m1.matrix[x][y] = 0
			for i := 0; i < 4; i++ {
				m1.matrix[x][y] += m1c[x][i] * m2.matrix[i][y]
			}
		}
	}

	return m1
}

func (m1 SpaceMatrix) Transpose() SpaceMatrix {
	m1c := *m1.matrix

	for x := 0; x < 4; x++ {
		for y := 0; y < 4; y++ {
			m1.matrix[x][y] = m1c[y][x]
		}
	}

	return m1
}

func (m1 SpaceMatrix) Translate(x, y, z gl.Float) SpaceMatrix {
	m1.matrix[0][3] += (m1.matrix[0][0] * x) + (m1.matrix[0][1] * y) + (m1.matrix[0][2] * z)
	m1.matrix[1][3] += (m1.matrix[1][0] * x) + (m1.matrix[1][1] * y) + (m1.matrix[1][2] * z)
	m1.matrix[2][3] += (m1.matrix[2][0] * x) + (m1.matrix[2][1] * y) + (m1.matrix[2][2] * z)
	m1.matrix[3][3] += (m1.matrix[3][0] * x) + (m1.matrix[3][1] * y) + (m1.matrix[3][2] * z)
	return m1
}

func (m1 SpaceMatrix) Scale(x, y, z gl.Float) SpaceMatrix {
	for i := 0; i < 4; i++ {
		m1.matrix[i][0] *= x
		m1.matrix[i][1] *= y
		m1.matrix[i][2] *= z
	}
	return m1
}

func (m1 SpaceMatrix) Rotate(x, y, z gl.Float) SpaceMatrix {
	var a = float64(x)
	var b = float64(y)
	var c = float64(z)

	var r00 = gl.Float(Cos(b) * Cos(c))
	var r01 = gl.Float(Cos(b) * Sin(c))
	var r02 = gl.Float(-Sin(b))
	var r10 = gl.Float(Sin(a)*Sin(b)*Cos(c) - Cos(a)*Sin(c))
	var r11 = gl.Float(Sin(a)*Sin(b)*Sin(c) + Cos(a)*Cos(c))
	var r12 = gl.Float(Sin(a) * Cos(b))
	var r20 = gl.Float(Cos(a)*Sin(b)*Cos(c) + Sin(a)*Sin(c))
	var r21 = gl.Float(Cos(a)*Sin(b)*Sin(c) - Sin(a)*Cos(c))
	var r22 = gl.Float(Cos(a) * Cos(b))

	mr := *m1.matrix

	m1.matrix[0][0] = (mr[0][0] * r00) + (mr[0][1] * r10) + (mr[0][2] * r20)
	m1.matrix[0][1] = (mr[0][0] * r01) + (mr[0][1] * r11) + (mr[0][2] * r21)
	m1.matrix[0][2] = (mr[0][0] * r02) + (mr[0][1] * r12) + (mr[0][2] * r22)

	m1.matrix[1][0] = (mr[1][0] * r00) + (mr[1][1] * r10) + (mr[1][2] * r20)
	m1.matrix[1][1] = (mr[1][0] * r01) + (mr[1][1] * r11) + (mr[1][2] * r21)
	m1.matrix[1][2] = (mr[1][0] * r02) + (mr[1][1] * r12) + (mr[1][2] * r22)

	m1.matrix[2][0] = (mr[2][0] * r00) + (mr[2][1] * r10) + (mr[2][2] * r20)
	m1.matrix[2][1] = (mr[2][0] * r01) + (mr[2][1] * r11) + (mr[2][2] * r21)
	m1.matrix[2][2] = (mr[2][0] * r02) + (mr[2][1] * r12) + (mr[2][2] * r22)

	m1.matrix[3][0] = (mr[3][0] * r00) + (mr[3][1] * r10) + (mr[3][2] * r20)
	m1.matrix[3][1] = (mr[3][0] * r01) + (mr[3][1] * r11) + (mr[3][2] * r21)
	m1.matrix[3][2] = (mr[3][0] * r02) + (mr[3][1] * r12) + (mr[3][2] * r22)

	return m1
}

func (m1 SpaceMatrix) Mirror(x, y, z gl.Float) SpaceMatrix {
	x, y, z = normalize(x, y, z)
	var r00 = 1 - 2*x*x
	var r01 = -2 * x * y
	var r02 = -2 * x * z
	var r10 = -2 * x * y
	var r11 = 1 - 2*y*y
	var r12 = -2 * y * z
	var r20 = -2 * x * z
	var r21 = -2 * y * z
	var r22 = 1 - 2*z*z

	mr := *m1.matrix

	m1.matrix[0][0] = (mr[0][0] * r00) + (mr[0][1] * r10) + (mr[0][2] * r20)
	m1.matrix[0][1] = (mr[0][0] * r01) + (mr[0][1] * r11) + (mr[0][2] * r21)
	m1.matrix[0][2] = (mr[0][0] * r02) + (mr[0][1] * r12) + (mr[0][2] * r22)

	m1.matrix[1][0] = (mr[1][0] * r00) + (mr[1][1] * r10) + (mr[1][2] * r20)
	m1.matrix[1][1] = (mr[1][0] * r01) + (mr[1][1] * r11) + (mr[1][2] * r21)
	m1.matrix[1][2] = (mr[1][0] * r02) + (mr[1][1] * r12) + (mr[1][2] * r22)

	m1.matrix[2][0] = (mr[2][0] * r00) + (mr[2][1] * r10) + (mr[2][2] * r20)
	m1.matrix[2][1] = (mr[2][0] * r01) + (mr[2][1] * r11) + (mr[2][2] * r21)
	m1.matrix[2][2] = (mr[2][0] * r02) + (mr[2][1] * r12) + (mr[2][2] * r22)

	m1.matrix[3][0] = (mr[3][0] * r00) + (mr[3][1] * r10) + (mr[3][2] * r20)
	m1.matrix[3][1] = (mr[3][0] * r01) + (mr[3][1] * r11) + (mr[3][2] * r21)
	m1.matrix[3][2] = (mr[3][0] * r02) + (mr[3][1] * r12) + (mr[3][2] * r22)

	return m1
}

func normalize(a, b, c gl.Float) (d, e, f gl.Float) {
	length := gl.Float(Sqrt(float64((a * a) + (b * b) + (c * c))))
	d = a / length
	e = b / length
	f = c / length
	return
}
