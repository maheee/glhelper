package glhelper

import (
	gl "gogl"
)

/*
 * Base Type
 */
type Uniform struct {
	program  gl.Uint
	name     string
	location gl.Int
}

/*
 * Special Types
 */
/*1*/
type Uniform1f Uniform

func (u *Uniform1f) Set(value gl.Float) {
	gl.Uniform1f(u.location, value)
}

type Uniform1i Uniform

func (u *Uniform1i) Set(value gl.Int) {
	gl.Uniform1i(u.location, value)
}

/*2*/
type Uniform2f Uniform

func (u *Uniform2f) Set(value1 gl.Float, value2 gl.Float) {
	gl.Uniform2f(u.location, value1, value2)
}

type Uniform2i Uniform

func (u *Uniform2i) Set(value1 gl.Int, value2 gl.Int) {
	gl.Uniform2i(u.location, value1, value2)
}

/*3*/
type Uniform3f Uniform

func (u *Uniform3f) Set(value1 gl.Float, value2 gl.Float, value3 gl.Float) {
	gl.Uniform3f(u.location, value1, value2, value3)
}

type Uniform3i Uniform

func (u *Uniform3i) Set(value1 gl.Int, value2 gl.Int, value3 gl.Int) {
	gl.Uniform3i(u.location, value1, value2, value3)
}

/*4*/
type Uniform4f Uniform

func (u *Uniform4f) Set(value1 gl.Float, value2 gl.Float, value3 gl.Float, value4 gl.Float) {
	gl.Uniform4f(u.location, value1, value2, value3, value4)
}

type Uniform4i Uniform

func (u *Uniform4i) Set(value1 gl.Int, value2 gl.Int, value3 gl.Int, value4 gl.Int) {
	gl.Uniform4i(u.location, value1, value2, value3, value4)
}

/*
 * Special Getter
 */
/*1*/
func (program *Program) GetUniform1f(name string) (*Uniform1f, error) {
	return &Uniform1f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniform1i(name string) (*Uniform1i, error) {
	return &Uniform1i{program.location, name, getUniformLocation(program, name)}, nil
}

/*2*/
func (program *Program) GetUniform2f(name string) (*Uniform2f, error) {
	return &Uniform2f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniform2i(name string) (*Uniform2i, error) {
	return &Uniform2i{program.location, name, getUniformLocation(program, name)}, nil
}

/*3*/
func (program *Program) GetUniform3f(name string) (*Uniform3f, error) {
	return &Uniform3f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniform3i(name string) (*Uniform3i, error) {
	return &Uniform3i{program.location, name, getUniformLocation(program, name)}, nil
}

/*4*/
func (program *Program) GetUniform4f(name string) (*Uniform4f, error) {
	return &Uniform4f{program.location, name, getUniformLocation(program, name)}, nil
}

func (program *Program) GetUniform4i(name string) (*Uniform4i, error) {
	return &Uniform4i{program.location, name, getUniformLocation(program, name)}, nil
}

/*
 * Universal Getter
 */

func getUniformLocation(program *Program, name string) gl.Int {
	nameGl := gl.GLString(name)
	defer gl.GLStringFree(nameGl)
	return gl.GetUniformLocation(program.location, nameGl)
}
