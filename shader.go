package glhelper

import (
	"errors"
	gl "gogl"
	"io/ioutil"
)

type Program struct {
	location gl.Uint
}

type Shader struct {
	location gl.Uint
}

type VertexShader Shader
type FragmentShader Shader

/*
 * Program functions
 */

func (program *Program) Use() {
	gl.UseProgram(program.location)
}

func (program *Program) Unuse() {
	gl.UseProgram(0)
}

/*
 * Delete functions
 */
func (shader *Shader) Delete() {
	gl.DeleteShader(shader.location)
}

func (program *Program) Delete() {
	gl.DeleteProgram(program.location)
}

/*func (attribute *Attribute) Location() gl.Uint {
	return attribute.location
}*/

/*
 * Logging
 */

func (shader *VertexShader) GetInfoLog() (res string) {
	return (*Shader)(shader).GetInfoLog()
}

func (shader *FragmentShader) GetInfoLog() (res string) {
	return (*Shader)(shader).GetInfoLog()
}

func (shader *Shader) GetInfoLog() (res string) {
	var logLength gl.Int
	var resultLength gl.Sizei

	gl.GetShaderiv(shader.location, gl.INFO_LOG_LENGTH, &logLength)
	if logLength == 0 {
		return
	}

	glResult := gl.GLStringAlloc(gl.Sizei(logLength))
	defer gl.GLStringFree(glResult)

	gl.GetShaderInfoLog(shader.location, gl.Sizei(logLength), &resultLength, glResult)
	if resultLength == 0 {
		return
	}
	return gl.GoStringN(glResult, resultLength)
}

func (program *Program) GetInfoLog() (res string) {
	var logLength gl.Int
	var resultLength gl.Sizei

	gl.GetProgramiv(program.location, gl.INFO_LOG_LENGTH, &logLength)
	if logLength == 0 {
		return
	}

	glResult := gl.GLStringAlloc(gl.Sizei(logLength))
	defer gl.GLStringFree(glResult)

	gl.GetProgramInfoLog(program.location, gl.Sizei(logLength), &resultLength, glResult)
	if resultLength == 0 {
		return
	}
	return gl.GoStringN(glResult, resultLength)
}

/*
 * Program creation
 */

func MakeProgram(vertexShader *VertexShader, fragmentShader *FragmentShader) (program *Program, err error) {
	program = &Program{location: gl.CreateProgram()}
	gl.AttachShader(program.location, vertexShader.location)
	gl.AttachShader(program.location, fragmentShader.location)
	gl.LinkProgram(program.location)

	var status gl.Int
	gl.GetProgramiv(program.location, gl.LINK_STATUS, &status)
	if status != gl.TRUE {
		err = errors.New("Creating program failed!")
	}

	return
}

func MakeVertexShader(filename string) (*VertexShader, error) {
	shaderLocation, err := makeShader(gl.VERTEX_SHADER, filename)
	shader := &VertexShader{location: shaderLocation}
	if err != nil {
		return shader, err
	}
	return shader, nil
}

func MakeFragmentShader(filename string) (*FragmentShader, error) {
	shaderLocation, err := makeShader(gl.FRAGMENT_SHADER, filename)
	shader := &FragmentShader{location: shaderLocation}
	if err != nil {
		return shader, err
	}
	return shader, nil
}

/*
 * Internals
 */

func readFile(filename string) (string, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(content), nil
}

func makeShader(t gl.Enum, filename string) (shader gl.Uint, err error) {
	source, err := readFile(filename)
	if err != nil {
		return 0, err
	}

	shader = gl.CreateShader(t)
	length := gl.Int(len(source))
	sourceGl := gl.GLString(source)
	defer gl.GLStringFree(sourceGl)
	gl.ShaderSource(shader, 1, &sourceGl, &length)
	gl.CompileShader(shader)

	var status gl.Int
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status != gl.TRUE {
		err = errors.New("Creating shader failed!")
	}

	return
}
